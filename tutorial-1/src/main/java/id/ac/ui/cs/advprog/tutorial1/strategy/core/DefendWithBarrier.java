package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    public DefendWithBarrier(){

    }
    public String getType(){
        return "Barrier";
    }
    public String defend(){
        return "Defend with Barrier";
    }
}
