package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    public DefendWithArmor(){

    }
    public String getType(){
         return "Armor";
    }
    public String defend(){
        return "Defend with Armor";
    }
}
