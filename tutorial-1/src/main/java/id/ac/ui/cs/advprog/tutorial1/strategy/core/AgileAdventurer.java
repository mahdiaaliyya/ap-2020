package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
    public AgileAdventurer(){
        AttackWithGun gun = new AttackWithGun();
        setAttackBehavior(gun);
        DefendWithBarrier barrier = new DefendWithBarrier();
        setDefenseBehavior(barrier);
    }
    public String getAlias() {
            return "Agile";
    }
}
