package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    public KnightAdventurer(){
        AttackWithSword sword = new AttackWithSword();
        setAttackBehavior(sword);

        DefendWithArmor armor = new DefendWithArmor();
        setDefenseBehavior(armor);
    }

    public String getAlias(){
        return "Knight";
    }
}
