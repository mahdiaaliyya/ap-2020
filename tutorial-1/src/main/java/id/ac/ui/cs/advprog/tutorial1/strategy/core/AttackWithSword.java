package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    public AttackWithSword() {

    }
    public String getType(){
        return "Sword";
    }
    public String attack(){
        return "Attack using Sword!";
    }
}
