package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    public MysticAdventurer(){
        AttackWithMagic magic = new AttackWithMagic();
        setAttackBehavior(magic);
        DefendWithShield shield = new DefendWithShield();
        setDefenseBehavior(shield);

    }

    public String getAlias(){
        return "Mystic";
    }
}
