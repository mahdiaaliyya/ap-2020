package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Sword;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegularUpgradeTest {

    private RegularUpgrade regularUpgrade;

    @BeforeEach
    public void setUp(){
        regularUpgrade = new RegularUpgrade(new Sword());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals("Sword", regularUpgrade.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        assertTrue(regularUpgrade.getDescription().contains("Regular Upgrade"));
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        //25+(1-5)
        assertTrue(26 <= regularUpgrade.getWeaponValue() && regularUpgrade.getWeaponValue() <= 30);
    }
}