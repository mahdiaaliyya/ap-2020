package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ShieldTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Shield();
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals("Shield", weapon.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        assertTrue(weapon.getDescription().contains("Heater Shield"));
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        // 10
        assertEquals(10, weapon.getWeaponValue());
    }
}