package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UniqueUpgradeTest {

    private UniqueUpgrade uniqueUpgrade;

    @BeforeEach
    public void setUp(){
        uniqueUpgrade = new UniqueUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        assertEquals("Shield", uniqueUpgrade.getName());
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        assertTrue(uniqueUpgrade.getDescription().contains("Unique Upgrade"));
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        //10 + (10-15)
        assertTrue(20 <= uniqueUpgrade.getWeaponValue() && uniqueUpgrade.getWeaponValue() <= 25);
    }
}