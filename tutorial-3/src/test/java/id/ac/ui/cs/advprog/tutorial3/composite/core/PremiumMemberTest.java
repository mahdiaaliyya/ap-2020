package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        int initialChildSize = member.getChildMembers().size();
        Member fathinah = new PremiumMember("Fathinah", "CEO");
        member.addChildMember(fathinah);
        assertEquals(initialChildSize + 1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member asma = new PremiumMember("Asma", "CEO");
        member.addChildMember(asma);
        int initialChildSize = member.getChildMembers().size();
        member.removeChildMember(asma);
        assertEquals(initialChildSize - 1, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member fathinah = new PremiumMember("Fathinah", "CEO");
        Member asma = new PremiumMember("Asma", "CEO");
        Member izzati = new PremiumMember("Izzati", "CEO");
        Member sakura = new PremiumMember("Sakura", "Merchant");
        member.addChildMember(fathinah);
        member.addChildMember(asma);
        member.addChildMember(izzati);
        member.addChildMember(sakura);
        assertEquals(3, member.getChildMembers().size());

    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member guildMaster = new PremiumMember("theMaster", "Master");
        Member fathinah = new PremiumMember("Fathinah", "CEO");
        Member asma = new PremiumMember("Asma", "CEO");
        Member izzati = new PremiumMember("Izzati", "CEO");
        Member sakura = new PremiumMember("Sakura", "Merchant");
        guildMaster.addChildMember(fathinah);
        guildMaster.addChildMember(asma);
        guildMaster.addChildMember(izzati);
        guildMaster.addChildMember(sakura);
        assertEquals(4, guildMaster.getChildMembers().size());
        assertEquals("Master", guildMaster.getRole());
    }
}