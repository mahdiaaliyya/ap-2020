package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        int membersBeforeAdd = guild.getMemberList().size();
        Member astrid = new OrdinaryMember("Astrid", "Designer");
        guild.addMember(guildMaster, astrid);
        assertEquals(membersBeforeAdd+1, guild.getMemberList().size());
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member upi = new OrdinaryMember("Upi", "Photographer");
        guild.addMember(guildMaster, upi);
        int membersBeforeRemove = guild.getMemberList().size();
        guild.removeMember(guildMaster, upi);
        assertEquals(membersBeforeRemove-1, guild.getMemberList().size());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member nasywa = new OrdinaryMember("Nasywa", "Analyst");
        guild.addMember(guildMaster, nasywa);
        assertEquals(nasywa, guild.getMember("Nasywa", "Analyst"));
    }
}