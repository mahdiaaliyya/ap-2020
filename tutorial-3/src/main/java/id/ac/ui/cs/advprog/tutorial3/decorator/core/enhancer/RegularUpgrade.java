package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {
    Weapon weapon;
    Random random = new Random();

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int upgradeValue = random.nextInt(4) + 1;
        return weapon.getWeaponValue() + upgradeValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription() + " Regular Upgrade.";
    }
}
