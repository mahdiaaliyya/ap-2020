package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    //TODO: Complete me
    String name;
    String role;
    List<Member> childMembers = new ArrayList<Member>();

    public PremiumMember(String name, String role){
        this.name = name;
        this.role = role;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        if (role.equals("Master")) {
            childMembers.add(member);
        }
        else if (childMembers.size() < 3) {
            childMembers.add(member);
        }
    }

    @Override
    public void removeChildMember(Member member) {
        childMembers.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return childMembers;
    }
}
