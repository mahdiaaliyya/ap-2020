package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.armor;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.ShiningArmor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ShiningArmorTest {

    Armor shiningArmor;

    @BeforeEach
    public void setUp(){
        shiningArmor = new ShiningArmor();
    }

    @Test
    public void testToString(){
        // TODO create test
        assertEquals("Shining Armor", shiningArmor.getName());
    }

    @Test
    public void testDescription(){
        // TODO create test
        assertTrue(shiningArmor.getDescription().contains("Shining Armor"));
    }
}
