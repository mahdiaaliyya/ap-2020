package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals(majesticKnight.getName(), "Majestic");
        assertEquals(metalClusterKnight.getName(), "Metal Cluster");
        assertEquals(syntheticKnight.getName(), "Synthetic");
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertEquals(null, majesticKnight.getSkill());
        assertEquals("Metal Armor", majesticKnight.getArmor().getName());
        assertEquals("Thousand Jacker", majesticKnight.getWeapon().getName());

        assertEquals(null, metalClusterKnight.getWeapon());
        assertEquals("Metal Armor", metalClusterKnight.getArmor().getName());
        assertEquals("Thousand Years Of Pain", metalClusterKnight.getSkill().getName());

        assertEquals(null, syntheticKnight.getArmor());
        assertEquals("Thousand Jacker", syntheticKnight.getWeapon().getName());
        assertEquals("Thousand Years Of Pain", syntheticKnight.getSkill().getName());
    }

}
