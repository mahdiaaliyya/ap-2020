package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        lordranAcademy = new LordranAcademy();
        majesticKnight = lordranAcademy.getKnight("majestic");
        metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        syntheticKnight = lordranAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals("Majestic", majesticKnight.getName());
        assertEquals("Metal Cluster", metalClusterKnight.getName());
        assertEquals("Synthetic", syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertEquals(null, majesticKnight.getSkill());
        assertEquals("Shining Armor", majesticKnight.getArmor().getName());
        assertEquals("Shining Buster", majesticKnight.getWeapon().getName());

        assertEquals(null, metalClusterKnight.getWeapon());
        assertEquals("Shining Armor", metalClusterKnight.getArmor().getName());
        assertEquals("Shining Force", metalClusterKnight.getSkill().getName());

        assertEquals(null, syntheticKnight.getArmor());
        assertEquals("Shining Buster", syntheticKnight.getWeapon().getName());
        assertEquals("Shining Force", syntheticKnight.getSkill().getName());

    }
}
