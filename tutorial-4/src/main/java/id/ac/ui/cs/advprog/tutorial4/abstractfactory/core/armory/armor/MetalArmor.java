package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor;

public class MetalArmor implements Armor {
    String name;
    String description;

    @Override
    public String getName() {
        // TODO fix me
        return "Metal Armor";
    }

    @Override
    public String getDescription() {
        // TODO fix me
        return "Metal Armor type Armory";
    }
}
