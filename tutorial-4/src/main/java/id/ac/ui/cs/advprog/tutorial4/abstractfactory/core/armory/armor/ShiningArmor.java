package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor;

public class ShiningArmor implements Armor {
    String name;
    String description;

    @Override
    public String getName() {
        // TODO fix me
        return "Shining Armor";
    }

    @Override
    public String getDescription() {
        // TODO fix me
        return "Shining Armor type Armory";
    }
}
