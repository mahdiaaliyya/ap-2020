package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon;

public class ShiningBuster implements Weapon {
    String name;
    String description;

    @Override
    public String getName() {
        // TODO fix me
        return "Shining Buster";
    }

    @Override
    public String getDescription() {
        // TODO fix me
        return "Shining Buster Weapon";
    }
}
